#!/bin/sh
set -e
scriptdir=${0%/*}
export TERM=linux

debug() {
	echo "$@"
	"$@"
}

run_test() {
	local infile=$1
	local ofile1=$(mktemp --suffix .wav)
	local ofile2=$(mktemp --suffix .wav)
	rm -rf "${ofile1}" "${ofile2}"
	echo "======================================================"
	echo "=== ${infile}"
	echo "======================================================"


	"${scriptdir}/lop.sh" "${infile}" "${ofile1}"
	"${scriptdir}/lop.py" "${infile}" "${ofile2}"

	ls -l "${ofile1}" "${ofile2}"
	md5sum "${ofile1}" "${ofile2}"
	diff -q "${ofile1}" "${ofile2}"

	rm -rf "${ofile1}" "${ofile2}"
	echo ""
}

for f in "$@"; do
	run_test "${f}"
done
